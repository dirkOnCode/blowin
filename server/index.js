// Setup basic express server
var express = require('express');
var app = express();


// now we need to parse body to receive commands and messages
var bodyParser = require('body-parser');
// var multer = require('multer'); 
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
// app.use(multer()); // for parsing multipart/form-data

var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
// the env.USER is not set when start node server as service
var user = process.env.USER || "pi";

// RPi B+ and RPi2+3 and Zero
var gpioPortWind            = process.env.GPIO || 17;   // PIN 11
var gpioPortQueueStoppedLed = process.env.GPIO || 27;   // PIN 13
var gpioPortQueueRunningLed = process.env.GPIO || 22;   // PIN 15

// am'I the Raspberry User?
var isRaspberryUser = (user === "pi" || user === "root");       

// queue controler
var EventEmitter;
var queueEventEmitter;

// message when server start to listen
server.listen(port, function() {
    console.log('user:' + user);
    console.log('Server listening at port %d', port);
    console.log("gpioPortWind = %d", gpioPortWind);
    console.log("gpioPortQueueStoppedLed  = %d", gpioPortQueueStoppedLed);
    console.log("gpioPortQueueRunningLed  = %d", gpioPortQueueRunningLed);

    EventEmitter = require("events").EventEmitter;
    queueEventEmitter = new EventEmitter();

    /*
     * run the next wind command
     */
    queueEventEmitter.on(windControl.run, function(nextWind) {
        setCurrentWindStatus(true);
        setTimeout(function() {
            setCurrentWindStatus(false);
            currentWind = null;
            addToWindQueueHistory(nextWind);
        }, 500);

    });

    function addToWindQueueHistory(nextWind) {
        windQueueHistory.unshift(nextWind);
        if(windQueueHistory.length > windQueueHistoryMaxLength) {
            windQueueHistory.length = windQueueHistoryMaxLength;
        }
        io.emit(clientMessage.readQueuedHistory, windQueueHistory );
    }

    function setCurrentWindStatus(status) {
        if( currentWindStatus !== status) {
            currentWindStatus = status;
            wind.writeSync(currentWindStatus ? 1 : 0);
            io.emit(clientMessage.windReportStatusChanged, {wind : currentWindStatus} );
        }
    }

    /*
     * add next command to queue
     * this is the server side listener for adding to queue
     */
    queueEventEmitter.on(windCommands.adminQueueAdd, function (msg) {
        windQueue.push({date: new Date(), message: msg});
        var echoText = "added wind" + (windQueueRunning ? "" : " (BUT Queue is stopped)") + ": ";
        io.emit(clientMessage.commandEcho, echoText +  msg);
    });

    /*
     * clear queue
     * this is the server side listener for clearing the queue
     */
    queueEventEmitter.on(windCommands.adminQueueClear, function (msg) {
        windQueue.length = 0;
        io.emit(clientMessage.commandEcho, "queue cleared " +  msg);
    });

    /*
     * stop queue
     * this is the server side listener for stopping the queue
     */
    queueEventEmitter.on(windCommands.adminQueueStop, function (msg) {
        windQueueController(false);
        io.emit(clientMessage.commandEcho, "queue stopped " +  msg);
    });

    /*
     * start queue
     * this is the server side listener for starting the queue
     */
    queueEventEmitter.on(windCommands.adminQueueStart, function (msg) {
        windQueueController(true);
        io.emit(clientMessage.commandEcho, "queue started " +  msg);
    });

    /*
     * queue worker
     * send next wind if queue is not empty and currentWind is done
     */
    var queueWorkerInterval = setInterval(function() {
        if (windQueueRunning && windQueue.length && !currentWind) {
            currentWind = windQueue.shift();
            queueEventEmitter.emit(windControl.run, currentWind);
        }
    }, 1000);

    function windQueueController(running) {
        windQueueRunning = running;
        queueStoppedLed.writeSync(running ? 0 : 1);      // red on when stopped
        queueRunningLed.writeSync(running ? 1 : 0);    // green on when running
        
    }
    
    // init queue status
    queueEventEmitter.emit(windCommands.adminQueueStart, "start queue");



});



// rest api for easy messaging from server to server
app.post('/rest/queue/', function (req, res) {
    var source = req.body.source || "guest";
    queueEventEmitter.emit(windCommands.adminQueueAdd, source);
    res.sendStatus(202);
});

// Routing, show index.html in /public folder
app.use(express.static(__dirname + '/public'));


/*
 * blow-in handler
 * here starts the main handling of the blow-in events
 */

/*
 * windQueue contains all the commands for doing the wind
 */
var windQueue = [];

/*
 * windQueueHistory contains last commands in reverse order with date
 */
var windQueueHistory = [];

/*
 * windQueueHistoryMaxLength limits the History
 */
var windQueueHistoryMaxLength = 100;

/*
 * currentWind points to the winds whats now active
 */
var currentWind;

/*
 * currentWind Status: on/off
 */
var currentWindStatus = false;


/*
 * windQueueRunning controlls the queue activity, false means queue is stopped
 */
var windQueueRunning = true;

/*
 * windCommands contains all valid admin queue commands
 */
var windCommands = {
    adminQueueAdd : 'blow-in admin queue add'
    ,adminQueueStart:  'blow-in admin queue start'
    ,adminQueueStop:  'blow-in admin queue stop'
    ,adminQueueClear:  'blow-in admin queue clear'
    ,adminQueueList:  'blow-in admin queue list'
    ,adminQueueHistoryList:  'blow-in admin queue history list'
    ,adminQueueHelp:  'blow-in help'
};


/*
 * clientMessage contains valid messages for the client 
 */
var clientMessage = {
    windReportStatusChanged : "blow-in windReport status changed"
    ,commandEcho : "blow-in commandEcho"
    ,readQueuedHistory : "blow-in readQueueHistory"
};

/*
 * windControl contains all commands to control the wind power
 */
var windControl = {
    run : "blow control run"
    ,calm : "calm"
    ,breath : "breath"
    ,breeze : "breeze"
    ,storm : "storm"
    ,hurricane : "hurricane"
};

/*
 * GPIOMock is used when you test this code on another system than a raspberry
 */
function GPIOMock (port,mode) {
    var myPort = port;
    this.writeSync = function(onoff) {
        var dateString = new Date().toISOString();
        console.log(timestampString() + myPort + " > mock writeSync " + onoff);
        return;
    };
    this.unexport = function() {
        console.log("\nmock unexport: ");
        return;
    };
}

/*
 * am'I runnuing on a Pi?
 */
if( isRaspberryUser ) {
    // yes, so inti gpio
    var GPIO = require('onoff').Gpio;
}
else {
    // no, so create an gpio mock, when not on Pi
    var GPIO = GPIOMock;
    console.log("not a RaspberryUser ? enable GPIO Mock ");
}

// set wind port to output
var wind = new GPIO(gpioPortWind, 'out');
var queueStoppedLed = new GPIO(gpioPortQueueStoppedLed, 'out');
var queueRunningLed = new GPIO(gpioPortQueueRunningLed, 'out');

/*
 * socket io handler
 */
io.on('connection', function(socket) {

    io.emit(clientMessage.readQueuedHistory, windQueueHistory );

    // listen to controls
    socket.on('blow-in control', function(msg) {
        switch(msg) {
            case "calm":
                queueEventEmitter.emit(windCommands.adminQueueClear, "calm");
                break;
            case "breath":
                queueEventEmitter.emit(windCommands.adminQueueAdd, "breath");
                break;
            case "breeze":
                queueEventEmitter.emit(windCommands.adminQueueAdd, "breeze1");
                queueEventEmitter.emit(windCommands.adminQueueAdd, "breeze2");
                break;
            case "storm":
                queueEventEmitter.emit(windCommands.adminQueueAdd, "strom1");
                queueEventEmitter.emit(windCommands.adminQueueAdd, "strom2");
                queueEventEmitter.emit(windCommands.adminQueueAdd, "strom3");
                queueEventEmitter.emit(windCommands.adminQueueAdd, "strom4");
                break;
            case "blowin":
                queueEventEmitter.emit(windCommands.adminQueueStart, "blowIn");
                break;
            case "lull":
                queueEventEmitter.emit(windCommands.adminQueueStop, "lulled");
                break;
            default:
                console.log("unknown command:" + msg);
        }        
        io.emit(clientMessage.commandEcho, msg + " - done.");
    });



    /*
     * listen to commands over socket.io from client
     * client add a message to queue 
    */
    socket.on(windCommands.adminQueueAdd, function(msg) {
        // send to internal event listener
        queueEventEmitter.emit(windCommands.adminQueueAdd, msg);
    });

    socket.on(windCommands.adminQueueClear, function(msg) {
        queueEventEmitter.emit(windCommands.adminQueueClear, msg);
    });

    // get queue list
    socket.on(windCommands.adminQueueList, function(msg) {
        
        io.emit(clientMessage.commandEcho, windQueue);
    });
    // get history 
    socket.on(windCommands.adminQueueHistoryList, function(msg) {
        
        io.emit(clientMessage.readQueuedHistory, windQueueHistory );
    });

    // get help
    socket.on(windCommands.adminQueueHelp, function(msg) {
        io.emit(clientMessage.commandEcho, windCommands);
    });


    // handle queue control
    socket.on(windCommands.adminQueueStop, function(msg) {
        queueEventEmitter.emit(windCommands.adminQueueStop, msg);
    });
    socket.on(windCommands.adminQueueStart, function(msg) {
        queueEventEmitter.emit(windCommands.adminQueueStart, msg);
    });


});


function exit() {
  wind.unexport();
  process.exit();
}

process.on('SIGINT', exit);


// Helper function
function timestampString()
{
    return "[" + new Date().toISOString() + "] ";
}