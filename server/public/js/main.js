/* 
 Created on : 09.04.2015, 09:54:00
 Author     : dirk
 */

var socket = io();

$('form').submit(function() {
    var value = $('#m').val();
    var valueArray = value.split(":");
    var cmd = valueArray[0].trim();
    var opt = valueArray.length > 1 ? valueArray[1].trim() : "";
    
    socket.emit('blow-in ' + cmd, opt);
    
    $('#m').val('');
    return false;
});

socket.on('blow-in readQueueHistory', function(queueHistory) {
    $('#QueueHistory').empty();
    $(queueHistory).each(function(){
        $('#QueueHistory').append($('<li>').append(JSON.stringify(this)));
    });
});

socket.on('blow-in commandEcho', function(msg) {
    $('#commandEcho').prepend($('<li>').append(JSON.stringify(msg)));
});

$("a").click(function(event) {
    socket.emit('blow-in control', $(this).attr("rel"));

});


