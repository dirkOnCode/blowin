#!/bin/bash

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null
cd $SCRIPTPATH

echo "fetch and pull from remote"
git fetch
git pull

echo "done."

ps aux | grep "[/]home/pi/blowin/server/index.js"

echo "You may now kill this process to restart blowin"
echo "use: sudo kill "
