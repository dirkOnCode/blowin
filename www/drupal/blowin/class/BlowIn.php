<?php

/**
 * Description of BlowIn
 *
 * @author dirk
 */
class BlowIn {
    
    // default localhost for testing
    private $protocol;
    private $host;
    private $port;
    const REST_API_QUEUE = "/rest/queue/";
    const TIMEOUT = 1;

    public function __construct($protocol="http", $host="localhost", $port=80) {
        $this->protocol = $protocol;
        $this->host = $host;
        $this->port = $port;
    }

    public function send($timeout = 1) {
        
        $url = $this->getUrlWithPort() . self::REST_API_QUEUE;
        $context = $this->createContext($timeout);
        $result = file_get_contents($url, false, $context);
        return !($result === false);
    }
       
    private function createContext($timeout) {
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header' => "Content-type: application/json",
                'method' => 'POST',
                'timeout' => $timeout
            )
        );
        return stream_context_create($options);
    }
    
    public function getUrl() {
        return $this->protocol . "://" . $this->host;
    }

    public function getUrlWithPort() {
        return $this->getUrl() .":" . $this->port;
    }

    public function getProtocol() {
        return $this->protocol;
    }

    public function getHost() {
        return $this->host;
    }

    public function getPort() {
        return $this->port;
    }

    public function setProtocol($protocol) {
        $this->protocol = $protocol;
        return $this;
    }

    public function setHost($host) {
        $this->host = $host;
        return $this;
    }

    public function setPort($port) {
        $this->port = $port;
        return $this;
    }


}
