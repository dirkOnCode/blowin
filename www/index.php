<!doctype html>
<html>
    <head>
        <!--<meta name="viewport" content="width-device-width" />-->
        <meta name="viewport" content="width=320" />
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <title>blow-in remote</title>
    </head>
    <body>
        <h1>blow-in remote</h1>
        <p>By opening this page you have blowing the wind into the fablab zürich</p>

        <footer>
            Blow-In v0.1 by Dirk Altermatt
        </footer>
    </body>
</html>


<?php

require_once __DIR__ . '/class/BlowIn.php';

$blowIn = new BlowIn("http", "localhost", 3000);
$success = $blowIn->send();
echo '<p>' . ($success ?  'thanks for the wind' : 'no connection, no wind, server not ready, sorry :( try again ;)') . '</p>';


